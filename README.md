# c

Test project with:

* **Language:** C

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| SAST                | :white_check_mark: |
| Dependency Scanning | :x:                |
| Container Scanning  | :x:                |
| DAST                | :x:                |
| Code Quality        | :x:                |
| License Management  | :x:                |

